//
// Created by bso on 07.08.18.
//

#include <random>

#include <base/Logger.h>
#include "tl.pb.h"
#include "net/TcpTokenizer.h"

#include "TcpTokenizerTest.h"

enum Constant {
	SIMPLE_TEXT_SIZE = 15,

	LARGE_TEXT_SIZE = 100,
	PACKET_PORTION_SIZE = 13,

	MULTIPLE_PACKETS_COUNT = 3,
};

std::random_device _RD;
core::net::TcpTokenizer _Tokenizer;


char _MakeRandChar() {
	std::uniform_int_distribution<int> dist(1, 128);
	while(auto ch = dist(_RD)) {
		if((ch >= '0' && ch <= '9') ||
			 (ch >= 'a' && ch <= 'z') ||
			 (ch >= 'A' && ch <= 'Z')) {
			return static_cast<char>(ch);
		}
	}
}


std::string _MakeRandText(std::size_t size) {
	std::string rand_message;
	for(auto i = 0; i < size; ++i) {
		rand_message.push_back(_MakeRandChar());
	}
	return rand_message;
}


bool _IsEqual(const TcpPacket &p1, const TcpPacket &p2) {
	tlproto::TestMessage p1_message, p2_message;
	p1_message.ParseFromString(p1.m_data);
	p2_message.ParseFromString(p2.m_data);

	return p1.m_info.m_type == p2.m_info.m_type &&
				 p1.m_info.m_size == p2.m_info.m_size &&
				 p1.m_info.m_id == p2.m_info.m_id &&
				 p1_message.m_num() == p2_message.m_num() &&
				 p1_message.m_str() == p2_message.m_str();
}


std::vector<TcpPacket> _ProcessDataByParts(std::string data) {
	std::vector<TcpPacket> packets;
	while(!data.empty()) {
		auto portion_size = data.size() > PACKET_PORTION_SIZE ?
												PACKET_PORTION_SIZE : data.size();
		LOG_D("imitation of the incomplete part of the packet: portion_size = %",
					portion_size);
		auto tmp = _Tokenizer.ProcessData(data.data(), portion_size);
		packets.insert(packets.end(), tmp.begin(), tmp.end());
		data.erase(data.begin(), data.begin() + portion_size);
	}

	return packets;
}


std::pair<tlproto::TestMessage, TcpPacket> _MakeSrcPacket(std::size_t text_size) {
	tlproto::TestMessage src_message;
	src_message.set_m_num(_RD());
	src_message.set_m_str(_MakeRandText(text_size));
	TcpPacket src_packet {static_cast<uint16_t>(_RD()),
												tlproto::mTestMessage,
												static_cast<uint32_t>(src_message.ByteSizeLong()),
												src_message.SerializeAsString()};
	return {src_message, src_packet};
}


std::pair<tlproto::TestMessage, TcpPacket> _MakeRandSrcPacket(std::size_t max_text_size) {
	std::uniform_int_distribution<int> dist(1, static_cast<int>(max_text_size));
	tlproto::TestMessage src_message;
	src_message.set_m_num(_RD());
	src_message.set_m_str(_MakeRandText(static_cast<size_t>(dist(_RD))));
	TcpPacket src_packet {static_cast<uint16_t>(_RD()),
												tlproto::mTestMessage,
												static_cast<uint32_t>(src_message.ByteSizeLong()),
												src_message.SerializeAsString()};
	return {src_message, src_packet};
}


std::pair<tlproto::TestMessage, TcpPacket> _MakeEmptySrcPacket() {
	tlproto::TestMessage src_message;
	TcpPacket src_packet {static_cast<uint16_t>(_RD()),
												tlproto::mTestMessage,
												static_cast<uint32_t>(src_message.ByteSizeLong()),
												src_message.SerializeAsString()};
	return {src_message, src_packet};
}


void RunSimplePacketTest() {
	LOG_I("Run simple packet test");
	_Tokenizer.ClearBuffer();
	auto[src_message, src_packet] = _MakeSrcPacket(SIMPLE_TEXT_SIZE);

	LOG_D("simple protobuf packet: \n%", src_message);
	LOG_D("packet info: %", src_packet.m_info);

	auto data = _Tokenizer.MakeOutputData(src_packet.m_info.m_type,
																				src_packet.m_info.m_id,
																				src_packet.m_data);

	auto packets = _Tokenizer.ProcessData(data.data(), data.size());

	if(_Tokenizer.GetBufferSize()) {
		LOG_E("Data is parsed INCORRECTLY: the tokenizer buffer must be empty after processing");
	}

	if(packets.size() == 1) {
		tlproto::TestMessage dst_packet;
		dst_packet.ParseFromString(packets[0].m_data);
		LOG_D("packet after tokenizer processing: \n%", dst_packet);
		LOG_D("packet info after tokenizer processing: %", packets[0].m_info);

		if(_IsEqual(src_packet, packets[0])) {
			LOG_I("The tokenizer processed the data CORRECTLY");
		}
	} else {
		LOG_E("Data is parsed INCORRECTLY: empty result");
	}
}


void RunPortionPacketTest() {
	LOG_I("Run portion packet test");
	_Tokenizer.ClearBuffer();
	auto[src_message, src_packet] = _MakeSrcPacket(LARGE_TEXT_SIZE);

	LOG_D("large protobuf packet: \n%", src_message);
	LOG_D("packet info: %", src_packet.m_info);

	auto data = _Tokenizer.MakeOutputData(src_packet.m_info.m_type,
																				src_packet.m_info.m_id,
																				src_packet.m_data);

	auto packets = _ProcessDataByParts(data);

	if(_Tokenizer.GetBufferSize()) {
		LOG_E("Data is parsed INCORRECTLY: the tokenizer buffer must be empty after processing");
	}

	if(packets.size() == 1) {
		tlproto::TestMessage dst_packet;
		dst_packet.ParseFromString(packets[0].m_data);
		LOG_D("packet after tokenizer processing: \n%", dst_packet);
		LOG_D("packet info after tokenizer processing: %", packets[0].m_info);

		if(_IsEqual(src_packet, packets[0])) {
			LOG_I("The tokenizer processed the data CORRECTLY");
		}
	} else {
		LOG_E("Data is parsed INCORRECTLY: empty result");
	}
}


void RunMultiplePacketsTest() {
	LOG_I("Run multiple packets test");
	_Tokenizer.ClearBuffer();
	std::string data;
	std::vector<TcpPacket> src_packets;
	for(auto i = 0; i < MULTIPLE_PACKETS_COUNT; ++i) {
		auto[src_message, src_packet] = _MakeRandSrcPacket(SIMPLE_TEXT_SIZE);
		LOG_D("simple protobuf packet [%]: \n%", i, src_message);
		LOG_D("packet info [%]: %", i, src_packet.m_info);

		data.append(_Tokenizer.MakeOutputData(src_packet.m_info.m_type,
																					src_packet.m_info.m_id,
																					src_packet.m_data));
		src_packets.push_back(src_packet);
	}

	auto packets = _Tokenizer.ProcessData(data.data(), data.size());

	if(_Tokenizer.GetBufferSize()) {
		LOG_E("Data is parsed INCORRECTLY: the tokenizer buffer must be empty after processing");
	}

	if(packets.size() != src_packets.size()) {
		LOG_E("Data is parsed INCORRECTLY: processed % packets instead %",
					packets.size(),
					src_packets.size());
		return;
	}

	for(auto i = 0; i < packets.size(); ++i) {
		auto &dst_packet = packets[i];
		tlproto::TestMessage dst_message;
		dst_message.ParseFromString(dst_packet.m_data);
		LOG_D("packet [%] after tokenizer processing: \n%", i, dst_message);
		LOG_D("packet [%] info after tokenizer processing: %", i, packets[i].m_info);

		if(!_IsEqual(src_packets[i], packets[i])) {
			LOG_E("Data is parsed INCORRECTLY: empty result");
			return;
		}
	}

	LOG_I("The tokenizer processed the data CORRECTLY");
}


void RunMultiplePortionPacketsTest() {
	LOG_I("Run multiple portion packets test");
	_Tokenizer.ClearBuffer();
	std::string data;
	std::vector<TcpPacket> src_packets;
	for(auto i = 0; i < MULTIPLE_PACKETS_COUNT; ++i) {
		auto[src_message, src_packet] = _MakeSrcPacket(SIMPLE_TEXT_SIZE);
		LOG_D("simple protobuf packet [%]: \n%", i, src_message);
		LOG_D("packet info [%]: %", i, src_packet.m_info);

		data.append(_Tokenizer.MakeOutputData(src_packet.m_info.m_type,
																					src_packet.m_info.m_id,
																					src_packet.m_data));
		src_packets.push_back(src_packet);
	}

	auto packets = _ProcessDataByParts(data);

	if(_Tokenizer.GetBufferSize()) {
		LOG_E("Data is parsed INCORRECTLY: the tokenizer buffer must be empty after processing");
	}

	if(packets.size() != src_packets.size()) {
		LOG_E("Data is parsed INCORRECTLY: processed % packets instead %",
					packets.size(),
					src_packets.size());
		return;
	}

	for(auto i = 0; i < packets.size(); ++i) {
		auto &dst_packet = packets[i];
		tlproto::TestMessage dst_message;
		dst_message.ParseFromString(dst_packet.m_data);
		LOG_D("packet [%] after tokenizer processing: \n%", i, dst_message);
		LOG_D("packet [%] info after tokenizer processing: %", i, packets[i].m_info);

		if(!_IsEqual(src_packets[i], packets[i])) {
			LOG_E("Data is parsed INCORRECTLY: empty result");
			return;
		}
	}

	LOG_I("The tokenizer processed the data CORRECTLY");
}


void RunEmptyPacketTest() {
	LOG_I("Run empty packet test");
	_Tokenizer.ClearBuffer();
	auto[src_message, src_packet] = _MakeEmptySrcPacket();

	LOG_D("packet info: %", src_packet.m_info);

	auto data = _Tokenizer.MakeOutputData(src_packet.m_info.m_type,
																				src_packet.m_info.m_id,
																				src_packet.m_data);

	auto packets = _Tokenizer.ProcessData(data.data(), data.size());

	if(_Tokenizer.GetBufferSize()) {
		LOG_E("Data is parsed INCORRECTLY: the tokenizer buffer must be empty after processing");
	}

	if(packets.size() == 1) {
		tlproto::TestMessage dst_packet;
		dst_packet.ParseFromString(packets[0].m_data);
		LOG_D("packet after tokenizer processing: \n%", dst_packet);
		LOG_D("packet info after tokenizer processing: %", packets[0].m_info);

		if(_IsEqual(src_packet, packets[0])) {
			LOG_I("The tokenizer processed the data CORRECTLY");
		}
	} else {
		LOG_E("Data is parsed INCORRECTLY: empty result");
	}
}