//
// Created by bso on 30.07.18.
//

#pragma once

#include <base/IStateMachine.h>

using namespace core::base;

enum class StateMachineTestType {
	Standard = 0,
	IncorrectBehavior
};

namespace State {
enum T {
	StateDone = 0,
	StateOne,
	StateTwo
};
}

class StateMachineTest: public IStateContext<StateMachineTest> {
public:
	/**
	* Default ctor
	*/
	StateMachineTest() = default;

	/**
	* Run test
	*
	* If type == StateMachineTestType::Standard then:
	* State1 -> State2 -> StateDone (without incorrect behavior)
	*
	* If type == StateMachineTestType::IncorrectBehavior then:
	* `
	* @param test_type - test type: Standard or IncorrectBehavior
	*/
	void RunTest(StateMachineTestType test_type);

private:
	void onDisallowedState() final;

	std::string getStateName(unsigned int state_id) final;

	struct StateDone: public IState<StateMachineTest> {
		StateDone();

		void OnChanged() final;

		bool CheckNextStateAllowed(unsigned int next_state_id) final;
	};

	struct StateOne: public IState<StateMachineTest> {
		StateOne();

		void OnChanged() final;

		bool CheckNextStateAllowed(unsigned int next_state_id) final;
	};

	struct StateTwo: public IState<StateMachineTest> {
		explicit StateTwo(const std::string &some_text);

		void OnChanged() final;

		bool CheckNextStateAllowed(unsigned int next_state_id) final;
	};

	StateMachineTestType m_test_id;
};