set(SUBMODULE base-test)

add_executable(${SUBMODULE}
        main.cpp
        StateMachineTest.h
        StateMachineTest.cpp
        )

target_link_libraries(${SUBMODULE}
        libcore
        tlproto
        stdc++fs

        ${Boost_PROGRAM_OPTIONS_LIBRARY}
        )