//
// Created by bso on 30.07.18.
//

#include <base/Logger.h>
#include <base/Checking.h>

#include "StateMachineTest.h"

void StateMachineTest::RunTest(StateMachineTestType test_type) {
	m_test_id = test_type;
	switch(test_type) {
		case StateMachineTestType::Standard: {
			LOG_I(R"('Standard' test is began)");
			LOG_I(R"(Assumed path of states: "State1 -> State2 -> StateDone")");
			break;
		}

		case StateMachineTestType::IncorrectBehavior: {
			LOG_I(R"('Incorrect Behavior' test is began)");
			LOG_I(R"(Assumed path of states: "State1 -> State2 -> (invalid transition)");
			break;
		}
	}

	setState<StateOne>();
}


void StateMachineTest::onDisallowedState() {
	LOG_I("Incorrect change of state");
}


std::string StateMachineTest::getStateName(unsigned int state_id) {
	switch(state_id) {
		case State::StateDone: {
			return "StateDone";
		}
		case State::StateOne: {
			return "StateOne";
		}
		case State::StateTwo: {
			return "StateTwo";
		}
		default: {
			THROWEX_D("Unknown state_id");
			return "";
		}
	}
}


StateMachineTest::StateDone::StateDone(): IState(State::StateDone) {
	LOG_D("StateDone Ctr");
}


void StateMachineTest::StateDone::OnChanged() {
	LOG_D("StateDone::OnChanged()");
}


bool StateMachineTest::StateDone::CheckNextStateAllowed(unsigned int next_state_id) {
	return false;
}


StateMachineTest::StateOne::StateOne(): IState(State::StateOne) {
	LOG_D("StateOne Ctr");
}


void StateMachineTest::StateOne::OnChanged() {
	LOG_D("StateOne::OnChanged()");
	switch(m_ctx->m_test_id) {
		case StateMachineTestType::Standard: {
			m_ctx->setState<StateTwo>("Some text");
			break;
		}

		case StateMachineTestType::IncorrectBehavior: {
			m_ctx->setState<StateOne>();
			break;
		}
	}
}


bool StateMachineTest::StateOne::CheckNextStateAllowed(unsigned int next_state_id) {
	return next_state_id == State::StateTwo;
}


StateMachineTest::StateTwo::StateTwo(const std::string &some_text):
		IState(State::StateTwo) {
	LOG_D(R"(StateTwo Ctr. argument = '%')", some_text);
}


void StateMachineTest::StateTwo::OnChanged() {
	LOG_D("StateTwo::OnChanged()");
	m_ctx->setState<StateDone>();
}


bool StateMachineTest::StateTwo::CheckNextStateAllowed(unsigned int next_state_id) {
	return next_state_id == State::StateDone;
}
