//
// Created by bso on 25.07.18.
//

//#include <boost/test/unit_test.hpp>
#include <boost/program_options.hpp>

#include "base/Formatter.h"
#include "base/Logger.h"

#include "StateMachineTest.h"

namespace po = boost::program_options;

void RunStateMachineTest() {
	//auto test1 = std::make_shared<StateMachineTest>(),
	//		test2 = std::make_shared<StateMachineTest>();
	std::shared_ptr<StateMachineTest> test1(new StateMachineTest()),
			test2(new StateMachineTest());
	test1->RunTest(StateMachineTestType::Standard);
	LOG_ENDL();
	test2->RunTest(StateMachineTestType::IncorrectBehavior);
}

int main(int argc, char *argv[]) {
	std::string log_path;
	po::options_description general_options("General options");
	general_options.add_options()
			("help,h", "Show help")
			("log-path,l", po::value<std::string>(&log_path)->required(),
			 "Enter path where will logs");

	try {
		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, general_options), vm);
		if(vm.count("help")) {
			std::cout << general_options << std::endl;
			return 0;
		}

		po::notify(vm);
	} catch(const boost::program_options::error &ec) {
		std::cerr << ec.what();
		return 1;
	}

	Logger::ClearLogs();
	Logger::UseConsoleOutput();
	Logger::SetModuleName("base-test");
	Logger::UseLogFile(log_path);
	if(!Logger::InitLogger()) {
		std::cerr << Logger::GetLastError();
		return 1;
	}

	LOG_I("*** BASE-TEST started ***");

	try {
		RunStateMachineTest();
	} catch(const std::runtime_error &er) {
		LOG_E("Testing error: %", er.what());
	}

	LOG_I("*** BASE-TEST ended ***");
}
